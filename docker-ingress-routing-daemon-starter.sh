#!/bin/bash

/usr/bin/echo "[docker-ingress-routing-daemon-starter.sh] Waiting for docker swarm leader election to finish ..."
while /usr/bin/docker node ls 2>&1 | /usr/bin/grep -E "Leader" | /usr/bin/grep -q -v -E "Leader"; do
	/usr/bin/sleep 1
done
/usr/bin/echo "[docker-ingress-routing-daemon-starter.sh] Docker swarm leader elected: \"$(/usr/bin/docker node ls 2>&1 | /usr/bin/grep -E "Leader" | /usr/bin/sed s#*##g | /usr/bin/sed "s# \{1,\}# #g" | /usr/bin/cut -d " " -f 1,2)\""

/usr/bin/echo "[docker-ingress-routing-daemon-starter.sh] Waiting for docker ingress-sbox IPv4 address to be available ..."
while /usr/bin/docker inspect ingress --format '{{(index .IPAM.Config 0).Subnet}} {{index (split (index .Containers "ingress-sbox").IPv4Address "/") 0}}' | /usr/bin/cut -d " " -f 2 | /usr/bin/wc -c | /usr/bin/grep -q -E "1"; do
	/usr/bin/sleep 1
done
/usr/bin/echo "[docker-ingress-routing-daemon-starter.sh] Docker ingress-sbox IPv4 address available: $(/usr/bin/docker inspect ingress --format '{{(index .IPAM.Config 0).Subnet}} {{index (split (index .Containers "ingress-sbox").IPv4Address "/") 0}}' | /usr/bin/cut -d " " -f 2)"

/usr/bin/cat /opt/docker-ingress-routing-daemon/docker-ingress-routing-daemon | /usr/bin/sed "s#iptables#iptables -w#g" | /bin/bash -s -- --install --no-performance
